﻿
namespace DataStructures
{
    public interface IDoublyIterable<E> : IIterable<E>
    {
        IDoublyIterator<E> getDoublyIterator();
    }
}
