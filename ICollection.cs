﻿namespace DataStructures
{
    public interface ICollection<E> : IIterable<E>
    {
        int Size { get; }

        bool isEmpty();
        bool addElement(E element);
        bool hasElement(E element);
        bool removeElement(E element);
    }

}
