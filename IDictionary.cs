﻿namespace DataStructures
{
    public interface IDictionary<K, V>
    {
        ISet<K> keySet();
        bool hasKey(K key);
        V get(K key);
        bool put(K key, V value);
        bool remove(K key);
        bool removeAll();
    }

}
