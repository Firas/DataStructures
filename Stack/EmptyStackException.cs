﻿using System;

namespace DataStructures.Stack
{
    public class EmptyStackException : EmptyCollectionException
    {
        public EmptyStackException() : base("Trying to access elements from an empty stack") { }
        public EmptyStackException(String message) : base(message) { }
        public EmptyStackException(String message, Exception innerException) : base(message, innerException) { }
    }
}
