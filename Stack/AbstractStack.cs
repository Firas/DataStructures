﻿
namespace DataStructures.Stack
{
    public abstract class AbstractStack<E> : IStack<E>
    {
        public abstract int Size { get; }

        public abstract bool push(E element);
        public abstract E pop();
        public abstract E getTop();
        public abstract bool setTop(E element);
        public abstract bool hasElement(E element);
        public abstract bool removeElement(E element);
        public abstract IIterator<E> getIterator();

        public bool isEmpty()
        {
            return Size == 0;
        }

        public bool addElement(E element)
        {
            return push(element);
        }
        
    }
}
