﻿
namespace DataStructures.List
{
    public class LinkedListNode<E>
    {
        protected LinkedListNode<E> next;
        private E element;

        public LinkedListNode<E> Next {
            get {
                return next;
            }
            set {
                next = value;
            }
        }
        public E Element {
            get {
                return element;
            }
            set {
                element = value;
            }
        }
        public LinkedListNode(E element) : this(element, null) { }
        public LinkedListNode(E element, LinkedListNode<E> next)
        {
            this.Element = element;
            this.Next = next;
        }
    }
}
