﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures.List
{
    public class DoublyLinkedListNode<E>
    {
        protected DoublyLinkedListNode<E> next, prev;
        private E element;

        public DoublyLinkedListNode<E> Next {
            get {
                return next;
            }
            set {
                next = value;
            }
        }
        public DoublyLinkedListNode<E> Prev {
            get {
                return prev;
            }
            set {
                prev = value;
            }
        }
        public E Element {
            get {
                return element;
            }
            set {
                element = value;
            }
        }

        public DoublyLinkedListNode(E element) : this(element, null, null) { }
        public DoublyLinkedListNode(E element, DoublyLinkedListNode<E> next) : this(element, next, null) { }
        public DoublyLinkedListNode(E element, DoublyLinkedListNode<E> next, DoublyLinkedListNode<E> prev)
        {
            this.element = element;
            if (null != (this.next = next)) next.prev = this;
            if (null != (this.prev = prev)) prev.next = this;
        }
    }
}
