﻿using System;

namespace DataStructures.List
{
    public class DoublyLinkedList<E> : AbstractList<E>
    {
        protected DoublyLinkedListNode<E> first;
        public override int Size
        {
            get
            {
                int size = 0;
                for (DoublyLinkedListNode<E> node = first; node != null; node = node.Next)
                {
                    ++size;
                }
                return size;
            }
        }

        public override E getByIndex(int index)
        {
            checkIndexMin(index);
            checkEmpty();
            DoublyLinkedListNode<E> node = first;
            for (int i = 0; i < index && node != null; ++i) node = node.Next;
            if (null == node) throw new IndexOutOfRangeException();
            return node.Element;
        }

        public override bool insertBeforeFirst(E element)
        {
            first = new DoublyLinkedListNode<E>(element, first);
            return true;
        }
        public override bool insertByIndex(int index, E element)
        {
            checkIndexMin(index);
            if (0 == index) return insertBeforeFirst(element);

            DoublyLinkedListNode<E> node = first;
            for (int i = 1; i < index && node != null; ++i) node = node.Next;
            if (node == null) throw new IndexOutOfRangeException();
            node.Next = new DoublyLinkedListNode<E>(element, node.Next);
            return true;
        }

        public override E removeFirst()
        {
            if (null == first) throw new IndexOutOfRangeException();
            E temp = first.Element;
            first = first.Next;
            return temp;
        }
        public override E removeByIndex(int index)
        {
            checkEmpty();
            checkIndexMin(index);
            if (0 == index) return removeFirst();
            DoublyLinkedListNode<E> node = first;
            for (int i = 1; i < index && node.Next != null; ++i) node = node.Next;
            if (node.Next == null) throw new IndexOutOfRangeException();
            E temp = node.Next.Element;
            node.Next = node.Next.Next;
            return temp;
        }

        public override bool setByIndex(int index, E element)
        {
            checkEmpty();
            checkIndexMin(index);
            DoublyLinkedListNode<E> node = first;
            for (int i = 0; i < index && node != null; ++i) node = node.Next;
            if (null == node) throw new IndexOutOfRangeException();
            node.Element = element;
            return true;
        }

        public override int indexOf(E element)
        {
            int result = 0;
            for (DoublyLinkedListNode<E> node = first; null != node; node = node.Next, ++result)
            {
                if (node.Element.Equals(element)) return result;
            }
            return -1;
        }
        public override int lastIndexOf(E element)
        {
            int result = -1, index = 0;
            for (DoublyLinkedListNode<E> node = first; null != node; node = node.Next, ++index)
            {
                if (node.Element.Equals(element)) result = index;
            }
            return result;
        }
        public override bool hasElement(E element)
        {
            return indexOf(element) >= 0;
        }

        protected class LinkekListIterator<E> : IIterator<E>
        {
            DoublyLinkedListNode<E> currentNode;

            public LinkekListIterator(DoublyLinkedListNode<E> firstNode)
            {
                currentNode = firstNode;
            }

            public bool hasNext()
            {
                return currentNode != null;
            }

            public E next()
            {
                E temp = currentNode.Element;
                currentNode = currentNode.Next;
                return temp;
            }
        }
        public override IIterator<E> getIterator()
        {
            return new LinkekListIterator<E>(first);
        }

    }
}
