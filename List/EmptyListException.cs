﻿using System;

namespace DataStructures.List
{
    public class EmptyListException : EmptyCollectionException
    {
        public EmptyListException() : base("Trying to access elements from an empty list") { }
        public EmptyListException(String message) : base(message) { }
        public EmptyListException(String message, Exception innerException) : base(message, innerException) { }
    }
}
