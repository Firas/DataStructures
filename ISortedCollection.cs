﻿
namespace DataStructures
{
    public interface ISortedCollection<E> : ICollection<E>
    {
        int getRank(E element);
        E getByRank(int rank);
    }
}
