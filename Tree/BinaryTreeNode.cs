﻿
namespace DataStructures.Tree
{
    public class BinaryTreeNode<E>
    {
        protected BinaryTreeNode<E> left, right, parent;
        private E element;
        public BinaryTreeNode<E> Left {
            get {
                return left;
            }
            set {
                left = value;
            }
        }
        public BinaryTreeNode<E> Right {
            get {
                return right;
            }
            set {
                right = value;
            }
        }
        public BinaryTreeNode<E> Parent {
            get {
                return parent;
            }
            set {
                parent = value;
            }
        }
        public E Element {
            get {
                return element;
            }
            set {
                element = value;
            }
        }

        public BinaryTreeNode(E element) : this(element, null) { }
        public BinaryTreeNode(E element, BinaryTreeNode<E> parent)
        {
            this.Element = element;
            this.Parent = parent;
            this.Left = this.Right = null;
        }
    }
}
