﻿
namespace DataStructures.Tree
{
    public class RedBlackNode<E> : ExtendedBinaryTreeNode<E>
    {
        public bool IsRed {
            get {
                return (flag & 2) == 2;
            }
            set {
                if (value) flag |= 2;
                else flag &= ~2;
            }
        }
        public bool IsBlack {
            get {
                return (flag & 2) == 0;
            }
            set {
                if (value) flag &= ~2;
                else flag |= 2;
            }
        }

        public RedBlackNode(E element) : this(element, null) { }
        public RedBlackNode(E element, RedBlackNode<E> parent) : base(element, parent)
        {
            if (null != parent) IsRed = true;
        }
    }
}
