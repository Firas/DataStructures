﻿using System;

namespace DataStructures.Tree
{
    public class RedBlackTree<E> : ISortedCollection<E>
    {
        protected RedBlackNode<E> root;

        public int Size
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool addElement(E element)
        {
            throw new NotImplementedException();
        }

        public E getByRank(int rank)
        {
            throw new NotImplementedException();
        }

        public IIterator<E> getIterator()
        {
            throw new NotImplementedException();
        }

        public int getRank(E element)
        {
            throw new NotImplementedException();
        }

        public bool hasElement(E element)
        {
            throw new NotImplementedException();
        }

        public bool isEmpty()
        {
            throw new NotImplementedException();
        }

        public bool removeElement(E element)
        {
            throw new NotImplementedException();
        }
    }
}
