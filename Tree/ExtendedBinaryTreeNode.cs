﻿
namespace DataStructures.Tree
{
    public class ExtendedBinaryTreeNode<E> : BinaryTreeNode<E>
    {
        protected int flag;
        public bool IsLeft {
            get {
                return (flag & 1) == 1;
            }
            set {
                if (value) flag |= 1;
                else flag &= ~1;
            }
        }
        public bool IsRight {
            get {
                return null != this.Parent && (flag & 1) == 0;
            }
            set {
                if (value) flag &= ~1;
                else flag |= 1;
            }
        }
        public new ExtendedBinaryTreeNode<E> Left {
            get {
                return (ExtendedBinaryTreeNode<E>)base.Left;
            }
            set {
                if (null != value) value.IsLeft = true;
                base.Left = value;
            }
        }
        public new ExtendedBinaryTreeNode<E> Right {
            get {
                return (ExtendedBinaryTreeNode<E>)base.Right;
            }
            set {
                if (null != value) value.IsRight = true;
                base.Right = value;
            }
        }
        public new ExtendedBinaryTreeNode<E> Parent {
            get {
                return (ExtendedBinaryTreeNode<E>)base.Parent;
            }
            set {
                if (null == value) IsLeft = false;
                base.Parent = value;
            }
        }

        public ExtendedBinaryTreeNode(E element) : this(element, null) { }
        public ExtendedBinaryTreeNode(E element, ExtendedBinaryTreeNode<E> parent) : base(element, parent) { }
    }
}
