﻿
namespace DataStructures
{
    public interface IStack<E> : ICollection<E>
    {
        bool push(E element);
        E getTop();
        bool setTop(E element);
        E pop();
    }
}
