﻿using System;

namespace DataStructures.Test
{
    public abstract class Test
    {
        public static void writeAssertion(Object result, String expectation)
        {
            System.Console.Out.WriteLine(result.ToString() + " == " + expectation);
        }
    }
}
