﻿using System;
using DataStructures.List;

namespace DataStructures.Test.List
{
    public class TestLinkedList : TestList
    {
        public static void test()
        {
            LinkedList<String> list = new LinkedList<string>();
            testEmptyList(list);
            testOneElement(list);
            testTwoElements(list);
            System.Console.Out.WriteLine();
        }
        
    }
}
