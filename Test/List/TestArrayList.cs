﻿using System;
using DataStructures.List;

namespace DataStructures.Test.List
{
    public class TestArrayList : TestList
    {
        public static void test()
        {
            testInvalidConstructor();
            ArrayList<String> list = new ArrayList<string>();
            testEmptyList(list);
            testOneElement(list);
            testTwoElements(list);
            System.Console.Out.WriteLine();
        }
        public static void testInvalidConstructor()
        {
            try
            {
                ArrayList<String> list = new ArrayList<string>(0);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                writeAssertion(ex.Message, "ArgumentOutOfRangeException");
            }
            try
            {
                ArrayList<String> list = new ArrayList<string>(1, 0);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                writeAssertion(ex.Message, "ArgumentOutOfRangeException");
            }
            System.Console.Out.WriteLine();
        }
    }
}
