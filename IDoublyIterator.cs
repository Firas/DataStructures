﻿
namespace DataStructures
{
    public interface IDoublyIterator<E> : IIterator<E>
    {
        bool hasPrev();
        E prev();
    }
}
