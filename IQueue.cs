﻿
namespace DataStructures
{
    public interface IQueue<E> : ICollection<E>
    {
        bool enqueue(E element);
        E dequeue();
        E getFirst();
        E getLast();
    }
}
