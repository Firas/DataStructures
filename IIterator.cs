﻿
namespace DataStructures
{
    public interface IIterator<E>
    {
        bool hasNext();
        E next();
    }
}
