﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataStructures
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        static void Main()
        {
            Test.List.TestArrayList.test();
            Test.List.TestLinkedList.test();
            // TODO: DoublyLinkedList, CircularLinkedList, DoublyCircularLinkedList
            // TODO: LinkedArrayList, DoublyLinkedArrayList, CircularLinkedArrayList, DoublyCircularLinkedArrayList

            // TODO: SkipList
            // TODO: RedBlackTree, BTree
        }
    }
}
