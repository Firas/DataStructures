﻿using System;

namespace DataStructures
{
    public class EmptyCollectionException : InvalidOperationException
    {
        public EmptyCollectionException() : base("Trying to access elements from an empty collection") { }
        public EmptyCollectionException(String message) : base(message) { }
        public EmptyCollectionException(String message, Exception innerException) : base(message, innerException) { }
    }
}
