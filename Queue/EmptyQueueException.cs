﻿using System;

namespace DataStructures.Queue
{
    public class EmptyQueueException : EmptyCollectionException
    {
        public EmptyQueueException() : base("Trying to access elements from an empty queue") { }
        public EmptyQueueException(String message) : base(message) { }
        public EmptyQueueException(String message, Exception innerException) : base(message, innerException) { }
    }
}
