﻿
namespace DataStructures.Queue
{
    public abstract class AbstractQueue<E> : IQueue<E>
    {
        public abstract int Size { get; }

        public abstract E dequeue();
        public abstract bool enqueue(E element);
        public abstract E getFirst();
        public abstract E getLast();
        public abstract bool hasElement(E element);
        public abstract bool removeElement(E element);
        public abstract IIterator<E> getIterator();

        public bool isEmpty()
        {
            return Size == 0;
        }

        public bool addElement(E element)
        {
            return enqueue(element);
        }

    }
}
