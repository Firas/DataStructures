﻿
namespace DataStructures
{
    public interface IIterable<E>
    {
        IIterator<E> getIterator();
    }
}
