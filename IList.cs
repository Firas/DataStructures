﻿
namespace DataStructures
{
    public interface IList<E> : IStack<E>, IQueue<E>
    {
        bool setByIndex(int index, E element);
        bool insertByIndex(int index, E element);
        E getByIndex(int index);
        E removeByIndex(int index);
        int indexOf(E element);
        int lastIndexOf(E element);
        bool setFirst(E element);
        bool setLast(E element);
        bool insertBeforeFirst(E element);
        bool insertAfterLast(E element);
        E removeFirst();
        E removeLast();
        E[] toArray();
    }
}
